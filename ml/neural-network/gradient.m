function [theta1_grad, theta2_grad] = gradient(theta1, theta2, X, y, lambda)
  m = size(y, 1);
  num_labels = size(theta2, 1);
  hidden_layer_size = size(theta1, 1);
  
  y_matrix = zeros(m, num_labels);
  for i = 1:m
    y_matrix(i, y(i)) = 1;
  endfor

  a2 = append_bias(sigmoid(X * theta1'));
  a3 = sigmoid(a2 * theta2');

  h = hypothesis(X, theta1, theta2);                  
  d3 = (a3-y_matrix);
  theta2_grad = 1/m*d3'*a2 + lambda/m * [ zeros(num_labels, 1), theta2(:, 2:end)];
  
  z2 = sigmoid(X * theta1');
  d2 = (d3*theta2)(:, 2:end) .* z2 .* (1 - z2);
  theta1_grad = 1/m*d2'*X + ...
    lambda/m * [ zeros(hidden_layer_size, 1), theta1(:, 2:end)];
end
