% COST function.
% Shows how accurate our model is based on current model parameters.

% Input:
% theta1 - layer 1 weight matrix.
% X - data input matrix.
% y - correct output vector.

% Output:
% cost - number that represents the cost (error) of our data with specified parameters theta and lambda.

function J = cost_function(theta1, theta2, X, y, lambda)
  y_matrix = zeros(m, num_labels);
  for i = 1:m
    y_matrix(i, y(i)) = 1;
  endfor

  h = hypothesis(X, theta1, theta2);
  J = -1/m * sum((y_matrix .* log(h) + (1-y_matrix) .* log(1-h))(:)) ...
      + lambda/(2*m) * ( sum((theta1 .* theta1)(:, 2:end)(:)) ...
                        + sum((theta2 .* theta2)(:, 2:end)(:)) );
endfunction
