function [theta1, theta2] = find_theta(X, y, num_labels, 
    hidden_layer_size, alpha, num_iters, lambda)
  m = length(y);
  num_features = size(X, 2);
  Xa = [ ones(m, 1), X ];
  
  theta1 = rand(hidden_layer_size, num_features + 1);
  theta2 = rand(num_labels, hidden_layer_size + 1);

  for i = 1:num_iters
    [theta1_grad, theta2_grad] = gradient(theta1, theta2, Xa, y, lambda);
    theta1 = theta1 - theta1_grad;
    theta2 = theta2 - theta2_grad;
  endfor
endfunction
