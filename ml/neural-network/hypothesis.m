% HYPOTHESIS function.
% Predicts the output values y based on the input values data X and model parameters.

% Input:
% X - data - (m x n) matrix.
% theta1 - weight matrix ((m + 1) x n) of first layer.
% theta2 - weight matrix (n x k)

% Output:
% predictions - output values that a calculated based on model parameters - (m x 1) vector

function [predictions] = hypothesis(X, theta1, theta2)
    predictions = sigmoid(append_bias(X * theta1') * theta2');
end