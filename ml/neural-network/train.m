file_path = fileparts(mfilename('fullpath'));
cd(file_path);
load ./train.mat

num_labels = 4;
hidden_layer_size = 5;
alpha = -0.3;
num_iterations = 100;
lambda = 1;

fprintf('Training the agent begin...\n');
[theta1, theta2] = find_theta(X, y, num_labels, 
  hidden_layer_size, alpha, num_iterations, lambda);

prediction = predict(X, theta1, theta2);
fprintf('Training Set Accuracy: %f\n', mean(double(prediction == y)) * 100);

save -6 theta.mat theta1 theta2; % MATLAB 6 compatible
fprintf('theta1 and theta2 are ready to use\n');