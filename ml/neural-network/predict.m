function prediction = predict(X, theta1, theta2)
  a2 = sigmoid(append_bias(X) * theta1');
  y_matrix = sigmoid(append_bias(a2) * theta2');
  [dummy, prediction] = max(y_matrix, [], 2);
endfunction
