% append 1 as bias nodes and compute the value of the next layer
function result = append_bias(matrix)
    m = size(matrix, 1);
    result = [ ones(m, 1), matrix ];
endfunction
