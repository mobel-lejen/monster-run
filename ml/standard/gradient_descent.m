% GRADIENT DESCENT function.
% Iteratively optimizes theta model parameters.

% X - training set.
% y - training output values.
% theta - model parameters.
function [theta] = gradient_descent(X, y, theta)
    
    ALPHA = 5;
    ITER = 1000;
    m = size(X, 1);
    
    for iter = 1:ITER
      diff = hypothesis(X, theta) - y;
      sum = diff(:);
      
      theta = theta - ALPHA/m * sum * X;
    endfor
    
end