% COST function.
% Shows how accurate our model is based on current model parameters.

% Input:
% X - data - (m x 2) matrix.
% theta - model parameters - (m x 1) vector.
% y - correct output - (2 x 1) vector.

% Output:
% cost - number that represents the cost (error) of our data with specified parameters theta.

function [cost] = cost_function(X, y, theta)
    
    m = size(X, 1);
    tmp = y * log(hypothesis(X, theta)) + (1-y) * log(1-hypothesis(X, theta));

    % Calculate current predictions cost.
    cost = -1/m * sum(tmp(:))
end