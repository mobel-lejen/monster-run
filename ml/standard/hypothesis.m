% HYPOTHESIS function.
% Predicts the output values y based on the input values data X and model parameters.

% Input:
% X - data - (m x 2) matrix.
% theta - model parameters - (m x 1) vector.

% Output:
% predictions - output values that a calculated based on model parameters - (2 x 1) vector

function [predictions] = hypothesis(X, theta)

    predictions = sigmoid(transpose(theta) * X);
    predictions = transpose(predictions);

end