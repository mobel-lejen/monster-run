from include.path.path_generator import PathGenerator
from include.const import *
from include.state import State
import pickle
import os

BASE_PATH = os.path.join(BASE_DIR, "assets")

PATH_GENERATOR_FILE = os.path.join(BASE_PATH, "path-generator.dat")

def get_path_generator():
    exists = os.path.isfile(PATH_GENERATOR_FILE)
    if not exists:
        set_path_generator()

    with open(PATH_GENERATOR_FILE, "rb") as file:
        return pickle.load(file)

def set_path_generator():
    with open(PATH_GENERATOR_FILE, "wb") as file:
        path_generator = PathGenerator(State(MAZE))
        pickle.dump(path_generator, file)

if __name__ == "__main__":
    set_path_generator()
