# Monster Run
Monster Run adalah game yang dibuat berbasis aplikasi desktop dengan teknik AI menggunakan *Machine Learning* dan *Adversial Search*.

## Optional arguments main.py

- -h, --help  
  Show this help message and exit
- -m {random, split, nomove}, --monster {random, split, nomove}  
  Monster movement strategy
- -p {shortest, ml}, --predictor {shortest, ml}  
  Your targeted exit predictor
- -sp, --show-path  
  Show the predicted path you're heading in
- -id {manual, onclick, while_playing, none}, --input-data {onclick, manual, while_playing, none}    
  Choose how to input data

By default, the game run using **split** as monster movement strategy, **ml** as your targeted exit predictor,
**not showing** the predicted path you're heading in, and **none** as how to input data (not input the data).        

If you want to input data, i recommend using **onclick** argument.           
  
## Credits
Kelompok: **Mobel Lejen**

Anggota: 
- Ahmad Fauzan Amirul Isnain
- Muhammad Fairuzi Teguh
- Yusuf Tri Ardho
