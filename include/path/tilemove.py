from include.const import *
from include.util import *

class TileMove:

    def __init__(self, tile, move):
        self.tile = tile
        self.move = move
        self.num = TileMove.convert_to_num(tile, move)

    @staticmethod
    def convert_to_num(tile, move):
        return ((tile[1] * COL_SIZE + tile[0]) * MOVEMENT_SIZE + MOVE_NUMBER[move])

    def make_move(self):
        return TileMove(add_pair(self.tile, self.move), self.move)

    def __eq__(self, other):
        if isinstance(other, TileMove):
            return self.num == other.num
        else:
            return NotImplemented

    def __repr__(self):
        return str(self.tile) + ' ' + str(self.move)

    def __lt__(self, other):
        return (self.tile, self.move) < (other.tile, other.move)
