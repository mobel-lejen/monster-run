from include.const import *
from include.util import *
from include.path.tilemove import TileMove
from collections import deque
from include.path.path import Path
from heapq import heappop, heappush

class PathGenerator:
    def __init__(self, state):
        self._state = state
        self._distance = [(TILEMOVE_SIZE * [(INF, INF)]) for i in range(TILEMOVE_SIZE)]
        self._parent = [(TILEMOVE_SIZE * [None]) for i in range(TILEMOVE_SIZE)]
        self._precompute_distance()

    def path(self, from_tilemove, to_tilemove):
        now_tilemove = to_tilemove
        result = []
        while now_tilemove != from_tilemove:
            result.append(now_tilemove)
            now_tilemove = self._parent[from_tilemove.num][now_tilemove.num]
        result.append(from_tilemove)
        return Path(result[::-1])

    """
    Return best path for each exit door, with the same order as ALL_EXIT_DOOR_POS.
    """
    def paths_to_exit(self, from_tilemove):
        paths = []
        for exit_door in ALL_EXIT_DOOR_POS:
            best_path = None
            for move in ALL_MOVEMENT:
                exit_tilemove = TileMove(exit_door, move)
                path = self.path(from_tilemove, exit_tilemove)
                if not best_path or path < best_path:
                    best_path = path
            paths.append(best_path)
        return paths

    def _uniform_cost(self, source_tilemove):
        if self._state.get_state(source_tilemove.tile) != WALL_VALUE:
            self._distance[source_tilemove.num][source_tilemove.num] = (0, 0)
            pq = []
            heappush(pq, ((0, 0), source_tilemove))
            while pq:
                now_dist, now_tilemove = heappop(pq)

                # Change movement
                for movement in ALL_MOVEMENT:
                    if movement == now_tilemove.move:
                        continue
                    next_tilemove = TileMove(now_tilemove.tile, movement)
                    if is_valid_tile(next_tilemove.tile) and\
                        self._state.is_not_wall_tile(next_tilemove.tile):
                        update_dist = add_pair(now_dist, (0, 1))
                        if self._distance[source_tilemove.num][next_tilemove.num] > update_dist:
                            self._distance[source_tilemove.num][next_tilemove.num] = update_dist
                            self._parent[source_tilemove.num][next_tilemove.num] = now_tilemove
                            heappush(pq, (update_dist, next_tilemove))

                # Move forward
                next_tilemove = now_tilemove.make_move()
                if is_valid_tile(next_tilemove.tile) and\
                    self._state.is_not_wall_tile(next_tilemove.tile):
                    update_dist = add_pair(now_dist, (1, 0))
                    if self._distance[source_tilemove.num][next_tilemove.num] > update_dist:
                        self._distance[source_tilemove.num][next_tilemove.num] = update_dist
                        self._parent[source_tilemove.num][next_tilemove.num] = now_tilemove
                        heappush(pq, (update_dist, next_tilemove))

    def _precompute_distance(self):
        for x in range(COL_SIZE):
            for y in range(ROW_SIZE):
                for m in ALL_MOVEMENT:
                    self._uniform_cost(TileMove((x, y), m))

    def _is_valid_tilemove(self, tilemove):
        return self._state.is_valid_tile(tilemove.tile)
