from include.const import *

class Path:
    def __init__(self, tilemoves=[]):
        self.tile_cost = 0
        self.move_cost = 0
        self.tilemoves = tilemoves
        self.tiles = []
        self.moves = []
        for tilemove in tilemoves:
            if len(self.tiles) > 0 and self.tiles[-1] != tilemove.tile:
                self.tile_cost += 1
            if len(self.moves) > 0 and self.moves[-1] != tilemove.move:
                self.move_cost += 1
            self.tiles.append(tilemove.tile)
            self.moves.append(tilemove.move)

    def append(self, tilemove):
        self.tilemoves.append(tilemove)
        self.tiles.append(tilemove.tile)
        self.moves.append(tilemove.move)

    def get_first_tile_move(self):
        for i in range(1, len(self.moves)):
            if self.moves[i] == self.moves[i-1]:
                return self.moves[i]
        return MOVE_NOWHERE

    def __repr__(self):
        return self.tilemoves.__repr__()

    def __lt__(self, other):
        return (self.tile_cost, self.move_cost) < (other.tile_cost, other.move_cost)
