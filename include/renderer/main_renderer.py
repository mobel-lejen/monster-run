from include import pygame
from include.const import *
from include.util import *

class MainRenderer:
    def __init__(self, state):
        self._display_surf = pygame.display.set_mode(
            (WINDOW_WIDTH, WINDOW_HEIGHT), pygame.HWSURFACE)
        display_rect = self._display_surf.get_rect()
        self._center = (display_rect.centerx, display_rect.centery)
        self._pict = {}
        self._pict[WALL_VALUE] = pygame.image.load(WALL_IMAGE).convert_alpha()
        self._pict[PLAYER_VALUE] = pygame.image.load(PLAYER_IMAGE).convert_alpha()
        self._pict[MONSTER1_VALUE] = pygame.image.load(MONSTER1_IMAGE).convert_alpha()
        self._pict[MONSTER2_VALUE] = pygame.image.load(MONSTER2_IMAGE).convert_alpha()

        self._state = state
        self._state.add_observer(self)

    def init_draw(self, state):
        pygame.display.set_caption('Monster Run')
        self._display_surf.fill(BACKGROUND_COLOR)
        pygame.display.flip()
        for r, row in enumerate(state):
            for c, element in enumerate(row):
                if element != EMPTY_VALUE:
                    self._display_surf.blit(self._pict[element], scale_to_px((c, r)))
        pygame.display.flip()

    def char_moved_in_px(self, char_value, from_px, to_px):
        self._display_surf.fill(BACKGROUND_COLOR, pygame.Rect((from_px, (SIZE_PX, SIZE_PX))))
        pict = self._pict[char_value]
        self._display_surf.blit(pict, to_px)
        pygame.display.update((pygame.Rect((from_px, (SIZE_PX, SIZE_PX))),
            pygame.Rect((to_px, (SIZE_PX, SIZE_PX)))))

    def _show_popup(self, message):
        background = pygame.Surface((POPUP_WIDTH, POPUP_HEIGHT), pygame.SRCALPHA)
        background.fill(POPUP_BACKGROUND_COLOR)
        pos_px_background = (self._center[0] - POPUP_WIDTH // 2,
            self._center[1] - POPUP_HEIGHT // 2)
        background_rect = pygame.Rect((pos_px_background, (POPUP_WIDTH, POPUP_HEIGHT)))
        self._display_surf.blit(background, background_rect)

        font = pygame.font.Font(None, POPUP_FONT_SIZE)
        lines_of_messages = message.split("\n")
        while lines_of_messages[-1] == "":
            lines_of_messages.pop()

        diff_height = split_text_height(len(lines_of_messages), POPUP_FONT_SIZE)
        for i, message in enumerate(lines_of_messages):
            text = font.render(message, True, WHITE)
            rect = text.get_rect(center=(self._center[0], self._center[1] + diff_height[i]))
            self._display_surf.blit(text, rect)
        pygame.display.update(background_rect)

    def show_end_popup(self, message):
        self._show_popup(message + "\n"
            + "\n"
            + "Press ESC to quit\n"
            + "Or Enter to play again")

    def render_path(self, path):
        rects = []
        for tile in path.tiles:
            scaled_tile = scale_to_px(tile)
            rect = pygame.Rect((scaled_tile, (SIZE_PX, SIZE_PX)))
            self._display_surf.fill(PATH_COLOR, rect)
            rects.append(rect)

        pygame.display.update(rects)

    def clear_path(self, path):
        rects = []
        for tile in path.tiles:
            scaled_tile = scale_to_px(tile)
            rect = pygame.Rect((scaled_tile, (SIZE_PX, SIZE_PX)))
            self._display_surf.fill(BACKGROUND_COLOR, rect)
            rects.append(rect)

        pygame.display.update(rects)

    def char_moved(self, char_value, from_coor, to_coor):
        pass
