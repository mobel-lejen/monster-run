from include.const import *
from include.path.tilemove import TileMove
from include.util import *
import scipy.io as sio
from compute_ml_data import generate_ml_input
import numpy as np

class MLPredictor:
    def __init__(self, state, path_generator):
        self._state = state
        self._state.add_observer(self)
        self._path_generator = path_generator
        self._read_theta()
        self.reset()

    def _read_theta(self):
        mat = sio.loadmat(os.path.join(BASE_DIR, 'ml/neural-network/theta.mat'))
        self._theta1 = mat['theta1']
        self._theta2 = mat['theta2']

    def reset(self):
        self._last_predict = None
        self._last_predict_still_valid = False

    def append_ones(self, layer):
        return np.hstack((np.ones((layer.shape[0], 1)), layer))

    def sigmoid(self, x):
        return 1/(1+np.exp(-x))

    def make_prediction(self):
        if not self._last_predict_still_valid:
            pathtile = self._state.get_pos_history(PLAYER_VALUE)
            X = np.array([generate_ml_input(pathtile, self._path_generator)])
            a2 = self.sigmoid(self.append_ones(X) @ self._theta1.T)
            a3 = self.sigmoid(self.append_ones(a2) @ self._theta2.T)
            prediction = np.argmax(a3, axis=1)[0]

            from_tile = self._state.get_pos(PLAYER_VALUE)
            from_move = self._state.get_move(PLAYER_VALUE)
            from_tilemove = TileMove(from_tile, from_move)
            exit_tilemove = TileMove(ALL_EXIT_DOOR_POS[prediction - 1], MOVE_NOWHERE)
            self._last_predict = self._path_generator.path(from_tilemove, exit_tilemove)
            self._last_predict_still_valid = True
        return self._last_predict

    def char_moved_in_px(self, char_value, from_px, to_px):
        pass

    def char_moved(self, char_value, from_coor, to_coor):
        if char_value == PLAYER_VALUE:
            self._last_predict_still_valid = False
