from include.const import *
from include.path.tilemove import TileMove
from include.util import *

class ShortestPredictor:
    def __init__(self, state, path_generator):
        self._path_generator = path_generator
        self._state = state
        self._state.add_observer(self)
        self.reset()

    def reset(self):
        self._last_predict = None
        self._last_predict_still_valid = False

    def make_prediction(self):
        if not self._last_predict_still_valid:
            tile = self._state.get_pos(PLAYER_VALUE)
            move = self._state.get_move(PLAYER_VALUE)
            tilemove = TileMove(tile, move)
            self._last_predict = sorted(self._path_generator.paths_to_exit(tilemove))[0]
            self._last_predict_still_valid = True
        return self._last_predict

    def char_moved_in_px(self, char_value, from_px, to_px):
        pass

    def char_moved(self, char_value, from_coor, to_coor):
        if char_value == PLAYER_VALUE:
            self._last_predict_still_valid = False
