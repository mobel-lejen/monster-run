from include.path.tilemove import TileMove
from include.const import *
from include.util import *

class SplitEngine:
    def __init__(self, state, predictor, path_generator):
        self._state = state
        self._predictor = predictor
        self._path_generator = path_generator
        self.reset()

    def reset(self):
        self._last_move = {}
        self._last_move_still_valid = False

    def generate_move(self):
        if not self._last_move_still_valid:
            self._last_move = self._get_target()
        return self._last_move

    def _get_target(self):
        path = self._predictor.make_prediction()
        mid_exit = middle_ceil_list(path.tilemoves)
        tiles = self._state.get_poses()
        moves = self._state.get_moves()
        player_tilemove = TileMove(tiles[PLAYER_VALUE], moves[PLAYER_VALUE])
        monster1_tilemove = TileMove(tiles[MONSTER1_VALUE], moves[MONSTER1_VALUE])
        monster2_tilemove = TileMove(tiles[MONSTER2_VALUE], moves[MONSTER2_VALUE])
        monster1_to_mid = self._path_generator.path(monster1_tilemove, mid_exit)
        monster2_to_mid = self._path_generator.path(monster2_tilemove, mid_exit)
        player_to_mid = self._path_generator.path(player_tilemove, mid_exit)
        monster1_to_player = self._path_generator.path(monster1_tilemove, player_tilemove)
        monster2_to_player = self._path_generator.path(monster2_tilemove, player_tilemove)
        target = {}
        if (player_tilemove.tile == PLAYER_START_PLACE and player_tilemove.move == MOVE_NOWHERE):
            pass
        elif monster1_to_mid < monster2_to_mid:
            if not self._state.is_valid_px_movement(PLAYER_VALUE, player_tilemove.move) and monster1_to_mid < player_to_mid:
                monster1_move = MOVE_NOWHERE
            else:
                monster1_move = monster1_to_mid.get_first_tile_move()
            target = {
                MONSTER1_VALUE: monster1_move,
                MONSTER2_VALUE: monster2_to_player.get_first_tile_move(),
            }
        else:
            if not self._state.is_valid_px_movement(PLAYER_VALUE, player_tilemove.move) and monster2_to_mid < player_to_mid:
                monster2_move = MOVE_NOWHERE
            else :
                monster2_move = monster2_to_mid.get_first_tile_move()
            target = {
                MONSTER1_VALUE: monster1_to_player.get_first_tile_move(),
                MONSTER2_VALUE: monster2_move,
            }
        return target

    def char_moved_in_px(self, char_value, from_px, to_px):
        pass

    def char_moved(self, char_value, from_coor, to_coor):
        self._last_move_still_valid = False
