import random
from include.const import CHANGE_MOVEMENT
from include.util import *

class RandomEngine:
    def __init__(self, state):
        self._state = state
        self._state.add_observer(self)
        self.reset()

    def reset(self):
        self._last_move = {
            MONSTER1_VALUE: random.choice(CHANGE_MOVEMENT),
            MONSTER2_VALUE: random.choice(CHANGE_MOVEMENT),
        }
        self._last_move_cnt = {
            MONSTER1_VALUE: 0,
            MONSTER2_VALUE: 0,
        }

    def generate_move(self):
        for monster_value in ALL_MONSTER_VALUES:
            last_move = self._last_move[monster_value]
            last_move_cnt = self._last_move_cnt[monster_value]
            if self._state.is_valid_px_movement(monster_value, last_move):
                if random.randint(0, 10) < 3 and last_move_cnt > SIZE_PX:
                    choice_set = tuple(set(CHANGE_MOVEMENT) -\
                        {last_move, opposite_move(last_move)})
                    move = random.choice(choice_set)
                    if self._state.is_valid_px_movement(monster_value, move):
                        self._last_move[monster_value] = move
                        self._last_move_cnt[monster_value] = 0
            else:
                self._last_move[monster_value] = random.choice(CHANGE_MOVEMENT)
                self._last_move_cnt[monster_value] = 0
            self._last_move_cnt[monster_value] += 1
        return self._last_move

    def char_moved_in_px(self, char_value, from_px, to_px):
        pass

    def char_moved(self, char_value, from_coor, to_coor):
        pass
