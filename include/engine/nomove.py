import random
from include.const import MOVE_NOWHERE
from include.util import *

class NoMoveEngine:
    def __init__(self, state):
        self._state = state
        self._state.add_observer(self)
        self._last_move = {}
        for monster_value in ALL_MONSTER_VALUES:
            self._last_move[monster_value] = MOVE_NOWHERE

    def reset(self):
        pass

    def generate_move(self):
        return self._last_move

    def char_moved_in_px(self, char_value, from_px, to_px):
        pass

    def char_moved(self, char_value, from_coor, to_coor):
        pass
