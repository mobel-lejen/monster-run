from include import pygame
from include.pygame import K_DOWN, K_UP, K_LEFT, K_RIGHT, K_ESCAPE, K_RETURN
import sys
from include.const import *
from include.util import *
import random

class MonsterRun:
    def __init__(self, state, renderer, engine, predictor):
        pygame.init()
        self._renderer = renderer
        self._engine = {}
        self._prev_move = {}
        self._cur_move = {}
        self._engine = engine
        self._predictor = predictor
        self._state = state
        self._state.add_observer(self)

    def _reset_game(self):
        self._prev_move = {
            PLAYER_VALUE: MOVE_NOWHERE,
            MONSTER1_VALUE: MOVE_NOWHERE,
            MONSTER2_VALUE: MOVE_NOWHERE,
        }
        self._cur_move = {
            PLAYER_VALUE: MOVE_NOWHERE,
            MONSTER1_VALUE: MOVE_NOWHERE,
            MONSTER2_VALUE: MOVE_NOWHERE,
        }
        self._engine.reset()
        self._state.reset()
        self._renderer.init_draw(self._state)
        self._running = False
        self._last_path = None
        self._predictor.reset()

    def new_game(self):
        running = False
        self._reset_game()

        self._running = True
        while self._running:
            events = pygame.event.get()
            keys = pygame.key.get_pressed()

            check_quit(events, keys)
            self._get_player_movement(keys)
            self._get_monster_movement()
            self._move_char()
            self._check_end_state()

            pygame.time.delay(DELAY)

    def _get_player_movement(self, keys):
        if keys[K_RIGHT]:
            self._change_char_movement(PLAYER_VALUE, MOVE_RIGHT)
        elif keys[K_LEFT]:
            self._change_char_movement(PLAYER_VALUE, MOVE_LEFT)
        elif keys[K_UP]:
            self._change_char_movement(PLAYER_VALUE, MOVE_UP)
        elif keys[K_DOWN]:
            self._change_char_movement(PLAYER_VALUE, MOVE_DOWN)

    def _check_end_state(self):
        if self._state.is_finish():
            self._renderer.show_end_popup("You win!")
            self._running = False

        if self._state.is_lose():
            self._renderer.show_end_popup("You lose!")
            self._running = False

    def _change_char_movement(self, char_value, move_dir):
        if self._cur_move[char_value] != move_dir:
            if self._prev_move[char_value] == MOVE_NOWHERE:
                self._prev_move[char_value] = self._cur_move[char_value]
            self._cur_move[char_value] = move_dir

    def _get_monster_movement(self):
        moves = self._engine.generate_move()
        for monster_value, move in moves.items():
            self._change_char_movement(monster_value, move)

    def _move_char(self):
        for char_value in ALL_CHAR_VALUES:
            if self._state.is_valid_px_movement(char_value, self._cur_move[char_value]):
                self._state.move_char_in_px(char_value, self._cur_move[char_value])
                self._prev_move[char_value] = MOVE_NOWHERE
            elif self._prev_move[char_value] != MOVE_NOWHERE:
                self._state.move_char_in_px(char_value, self._prev_move[char_value])
            elif char_value in ALL_MONSTER_VALUES:
                # Automatically random monster movement if get stuck
                for move in CHANGE_MOVEMENT:
                    if move == opposite_move(self._cur_move[char_value]):
                        continue
                    if self._state.is_valid_px_movement(char_value, move):
                        self._cur_move[char_value] = move
                        break
                else:
                    self._cur_move[char_value] = opposite_move(self._cur_move[char_value])
                self._state.move_char_in_px(char_value, self._cur_move[char_value])

    def char_moved_in_px(self, char_value, from_px, to_px):
        pass

    def char_moved(self, char_value, from_coor, to_coor):
        if char_value == PLAYER_VALUE:
            if self._last_path:
                self._renderer.clear_path(self._last_path)
            self._last_path = self._predictor.make_prediction()
            self._renderer.render_path(self._last_path)

    def getState(self):
        return self._state
