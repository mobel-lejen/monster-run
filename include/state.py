from copy import deepcopy
from include.const import *
from include.util import *

"""
Observable state.

The observer should implement:
- char_moved_in_px(char_value, from_px, to_px)
- char_moved(char_value, from_coor, to_coor)
"""
class State:
    def __init__(self, state):
        self._init_state = state
        self._init_pos = {}
        self._init_pos_px = {}
        self._fill_init_pos()
        self._observers = []
        self.reset()

    def _fill_init_pos(self):
        for y, row in enumerate(self._init_state):
            for x, element in enumerate(row):
                if element != EMPTY_VALUE and element != WALL_VALUE:
                    self._init_pos[element] = (x, y)
                    self._init_pos_px[element] = scale_to_px((x, y))

    def reset(self):
        self._state = deepcopy(self._init_state)
        self._pos = deepcopy(self._init_pos)
        self._pos_px = deepcopy(self._init_pos_px)
        self._move = {
            PLAYER_VALUE: MOVE_NOWHERE,
            MONSTER1_VALUE: MOVE_NOWHERE,
            MONSTER2_VALUE: MOVE_NOWHERE,
        }
        self._pos_history = {
            PLAYER_VALUE: [self._init_pos[PLAYER_VALUE]],
            MONSTER1_VALUE: [self._init_pos[MONSTER1_VALUE]],
            MONSTER2_VALUE: [self._init_pos[MONSTER2_VALUE]],
        }

    def get_state(self, coor):
        return self._state[coor[1]][coor[0]]

    def set_state(self, coor, char_value):
        self._state[coor[1]][coor[0]] = char_value

    def move_pos(self, from_coor, to_coor, char_value):
        if from_coor != to_coor:
            self.set_state(from_coor, EMPTY_VALUE)
            self.set_state(to_coor, char_value)
            self._pos[char_value] = to_coor
            self._pos_history[char_value].append(to_coor)
            for observer in self._observers:
                observer.char_moved(char_value, from_coor, to_coor)

    def is_in_px_boundary(self, pos_px):
        return 0 <= pos_px[0] and pos_px[0] + SIZE_PX < WINDOW_WIDTH and\
            0 <= pos_px[1] and pos_px[1] + SIZE_PX < WINDOW_HEIGHT

    def move_char_in_px(self, char_value, movement):
        if self.is_valid_px_movement(char_value, movement):
            from_pos_px = self._pos_px[char_value]
            to_pos_px = add_pair(from_pos_px, movement)
            self._pos_px[char_value] = to_pos_px
            from_pos = scale_to_round_nearest_coor(from_pos_px)
            to_pos = scale_to_round_nearest_coor(to_pos_px)
            self.move_pos(from_pos, to_pos, char_value)
            self._move[char_value] = movement

            for observer in self._observers:
                observer.char_moved_in_px(char_value, from_pos_px, to_pos_px)
            return True
        else:
            return False

    def is_valid_px_movement(self, char_value, movement):
        from_pos_px = self._pos_px[char_value]
        to_pos_px = add_pair(from_pos_px, movement)
        return self.is_in_px_boundary(to_pos_px) and not self.is_collide_with_wall(to_pos_px)

    def is_not_wall_tile(self, tile):
        return self.get_state(tile) != WALL_VALUE

    def is_collide_with_wall(self, pos_px):
        return (self.get_state(scale_to_exact_round_coor(pos_px, LOWER_LEFT_ROUNDING)) == 1
            or self.get_state(scale_to_exact_round_coor(pos_px, UPPER_LEFT_ROUNDING)) == 1
            or self.get_state(scale_to_exact_round_coor(pos_px, LOWER_RIGHT_ROUNDING)) == 1
            or self.get_state(scale_to_exact_round_coor(pos_px, UPPER_RIGHT_ROUNDING)) == 1)

    def is_finish(self):
        pos_px = self._pos_px[PLAYER_VALUE]
        return (pos_px[0] == 0 or pos_px[0] == WINDOW_WIDTH - SIZE_PX - 1
            or pos_px[1] == 0 or pos_px[1] == WINDOW_HEIGHT - SIZE_PX - 1)

    def is_lose(self):
        lose = False
        for monster_value in ALL_MONSTER_VALUES:
            pos_px = self._pos_px[monster_value]
            lose = lose or self._pos[PLAYER_VALUE] == scale_to_exact_round_coor(pos_px, LOWER_LEFT_ROUNDING) or\
                self._pos[PLAYER_VALUE] == scale_to_exact_round_coor(pos_px, UPPER_LEFT_ROUNDING) or\
                self._pos[PLAYER_VALUE] == scale_to_exact_round_coor(pos_px, LOWER_RIGHT_ROUNDING) or\
                self._pos[PLAYER_VALUE] == scale_to_exact_round_coor(pos_px, UPPER_RIGHT_ROUNDING)
        return lose

    def add_observer(self, observer):
        self._observers.append(observer)

    def get_pos(self, char_value):
        return self._pos[char_value]

    def get_poses(self):
        return self._pos

    def get_move(self, char_value):
        return self._move[char_value]

    def get_moves(self):
        return self._move

    def get_pos_history(self, char_value):
        return self._pos_history[char_value]

    def get_pos_histories(self):
        return self._pos_history

    def __str__(self):
        result = ""
        for row in self._state:
            for element in row:
                result += str(element)
            result += "\n"
        return result

    def __iter__(self):
        for row in self._state:
            yield row
