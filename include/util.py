from include.const import *

def add_pair(pair, diff):
    return (pair[0] + diff[0], pair[1] + diff[1])

def substract_pair(pair, diff):
    return (pair[0] - diff[0], pair[1] - diff[1])

def scale_to_px(coor_pair):
    return (SIZE_PX * coor_pair[0], SIZE_PX * coor_pair[1])

def scale_to_round_nearest_coor(px_pair):
    return ((px_pair[0] + SIZE_PX // 2) // SIZE_PX, (px_pair[1] + SIZE_PX // 2) // SIZE_PX)

def scale_to_exact_round_coor(px_pair, rounding):
    return ((px_pair[0] + rounding[0]) // SIZE_PX, (px_pair[1] + rounding[1]) // SIZE_PX)

def split_text_height(num, font_size):
    total = font_size * (num - 1)
    half = total // 2
    result = []
    for i in range(num):
      result.append(i * font_size - half)
    return result

def is_monster(char_value):
    return char_value != PLAYER_VALUE

def opposite_move(movement):
    return (movement[0] * (-1), movement[1] * (-1))

def is_valid_tile(tile):
    return 0 <= tile[0] and tile[0] < COL_SIZE and 0 <= tile[1] and tile[1] < ROW_SIZE

def check_quit(events, keys):
    from include.pygame import QUIT, K_ESCAPE
    from sys import exit

    for event in events:
        if event.type == QUIT:
            exit()
    if keys[K_ESCAPE]:
        exit()

def middle_ceil_list(lis):
    return lis[- len(lis) // 2]
