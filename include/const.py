import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
DATA_FILE_NAME = "data.json"

EMPTY_VALUE = 0
WALL_VALUE = 1
PLAYER_VALUE = 2
MONSTER1_VALUE = 9
MONSTER2_VALUE = 8
ALL_CHAR_VALUES = (PLAYER_VALUE, MONSTER1_VALUE, MONSTER2_VALUE)
ALL_MONSTER_VALUES = (MONSTER1_VALUE, MONSTER2_VALUE)

MAZE = [
    [1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1],
    [1,0,0,0,0,0,0,1,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,1],
    [1,0,1,0,1,1,0,0,0,1,0,1,0,0,0,1,1,1,1,0,1,1,1,0,1],
    [1,0,1,0,0,1,1,1,0,1,0,1,0,1,0,0,0,0,1,9,0,0,1,0,1],
    [1,0,1,1,0,0,0,1,1,1,0,1,0,1,0,1,0,1,1,0,1,0,1,0,1],
    [1,0,0,0,1,1,0,0,0,1,0,0,0,0,0,1,0,0,1,0,1,0,1,0,1],
    [1,0,1,0,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1,0,1,0,1,0,1],
    [0,0,0,1,0,0,0,0,0,0,0,0,2,0,0,0,0,0,1,0,0,0,1,0,0],
    [1,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,1,0,1,1,1,1,1],
    [1,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1],
    [1,0,1,1,0,1,1,0,1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,0,1],
    [1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1],
    [1,1,0,1,1,0,1,0,1,0,1,0,1,0,0,1,1,1,1,1,0,1,0,1,1],
    [1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1], 
    [1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1] ]

NORTH_DOOR = (12,0)
SOUTH_DOOR = (12,14)
WEST_DOOR = (0,7)
EAST_DOOR = (24,7)
PLAYER_START_PLACE = (12,7)


ROW_SIZE = len(MAZE)
COL_SIZE = len(MAZE[0])
TILE_SIZE = ROW_SIZE * COL_SIZE

INIT_POS = {}
ALL_EXIT_DOOR_POS = []
for r in range(ROW_SIZE):
    for c in range(COL_SIZE):
        if MAZE[r][c] in ALL_CHAR_VALUES:
            INIT_POS[MAZE[r][c]] = (c, r)
        elif (c == 0 or c == COL_SIZE - 1
            or r == 0 or r == ROW_SIZE - 1) and MAZE[r][c] == EMPTY_VALUE:
            ALL_EXIT_DOOR_POS.append((c, r))

SIZE_PX = 50

WINDOW_HEIGHT = ROW_SIZE * SIZE_PX
WINDOW_WIDTH = COL_SIZE * SIZE_PX

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BACKGROUND_COLOR = WHITE
PATH_COLOR = (129, 236, 236)

WALL_IMAGE = os.path.join(BASE_DIR, "assets/steel-block.png")
PLAYER_IMAGE = os.path.join(BASE_DIR, "assets/dora.png")
MONSTER1_IMAGE = os.path.join(BASE_DIR, "assets/monster1.png")
MONSTER2_IMAGE = os.path.join(BASE_DIR, "assets/monster2.png")

MOVE_LEFT = (-1, 0)
MOVE_UP = (0, -1)
MOVE_RIGHT = (1, 0)
MOVE_DOWN = (0, 1)
MOVE_NOWHERE = (0, 0)
ALL_MOVEMENT = (MOVE_LEFT, MOVE_UP, MOVE_RIGHT, MOVE_DOWN, MOVE_NOWHERE)
CHANGE_MOVEMENT = (MOVE_LEFT, MOVE_UP, MOVE_RIGHT, MOVE_DOWN)
MOVEMENT_SIZE = len(ALL_MOVEMENT)
MOVE_NUMBER = {
    MOVE_LEFT: 0,
    MOVE_UP: 1,
    MOVE_RIGHT: 2,
    MOVE_DOWN: 3,
    MOVE_NOWHERE: 4,
}
TILEMOVE_SIZE = TILE_SIZE * MOVEMENT_SIZE

DELAY = 5

UPPER_LEFT_ROUNDING = (0, 0)
LOWER_LEFT_ROUNDING = (0, SIZE_PX - 1)
UPPER_RIGHT_ROUNDING = (SIZE_PX - 1, 0)
LOWER_RIGHT_ROUNDING = (SIZE_PX - 1, SIZE_PX - 1)

POPUP_FONT_SIZE = 40
POPUP_WIDTH = 500
POPUP_HEIGHT = 300
POPUP_BACKGROUND_COLOR = (0, 0, 0, 200)

INF = 1000000000

DATA_PATH_FILE_NAME = 'data.json'
