from tkinter import *
from tkinter import messagebox
import ast
import include.const as const
from include.input_data.data_reader_and_writer import DataReaderAndWriter

class ManualInput:
   
   def __init__(self):
      self.data_reader_and_writer = DataReaderAndWriter()
      self.data_list = self.data_reader_and_writer.read()
      self.fields = 'Target Door', 'Movement Path'
      self.door_choices = {'South', 'North', 'West', 'East'}

   def fetch(self, entries):
      for entry in entries:
         field = entry[0]
         text  = entry[1].get()
         print('%s: "%s"' % (field, text)) 

   def makeform(self, root, fields, door_choices):
      entries = []
      for field in fields:
         row = Frame(root)
         lab = Label(row, width=15, text=field, anchor='w')
         row.pack(side=TOP, fill=X, padx=5, pady=5)
         lab.pack(side=LEFT)
         
         if (field == 'Target Door'):
             ent = StringVar(root)
             ent.set('North')
             popupMenu = OptionMenu(row, ent, *door_choices)
             popupMenu.pack(side=LEFT)
         else:
             ent = Entry(row)
             ent.pack(side=RIGHT, expand=YES, fill=X)
         
         entries.append((field, ent))

      return entries

   def show_error(self, message):
       messagebox.showinfo("Error", message)

   def add_data(self, data, ents):
       data_dict = {}
       goal_field = ents[0][1]
       path_field = ents[1][1]

       try:
           path = ast.literal_eval(path_field.get())
       except:
           self.show_error("Invalid movement format")
           return     
      
       if (not self.check_valid_movement_format(path)):
           self.show_error("Invalid movement format")
           return

       if (not self.check_valid_continuous_move(path)):
           self.show_error("Invalid movement path")
           return
          
       goal = goal_field.get()
       goal_coordinate = path[-1]
       
       if (not self.check_valid_last_movement(goal, goal_coordinate)):
           self.show_error("Last movement not equal to goal coordinate")
           return

       data_dict['Goal'] = goal
       data_dict['Path'] = path
       path_field.delete(0, END)

       self.data_list.append(data_dict)

   def check_valid_movement_format(self, path):
       if type(path) is list:
           for i in path:
               if not(type(i) is tuple) or (len(i) != 2):
                   return False
       else:
           return False
       return True

   def check_valid_last_movement(self, goal, goal_coordinate):
       if (goal == "North"):
         if (goal_coordinate != const.NORTH_DOOR):
            return False
       elif (goal == "South"):
         if (goal_coordinate != const.SOUTH_DOOR):
            return False
       elif (goal == "East"):
         if (goal_coordinate != const.EAST_DOOR):
            return False
       elif (goal == "West"):
         if (goal_coordinate != const.WEST_DOOR):
            return False
       return True

   def check_valid_continuous_move(self, path):
      current_x = path[0][0]
      current_y = path[0][1]
      
      if (self.check_move_through_wall(current_x, current_y)):
         return False

      for i in range(1, len(path)):
         next_x = path[i][0]
         next_y = path[i][1]

         if (abs(current_x - next_x) > 1 or abs(current_y - next_y > 1)):
            return False
         if (abs(current_x - next_x) == 1 and abs(current_y-next_y) == 1):
            return False
         if (abs(current_x - next_x) == 0 and abs(current_y-next_y) == 0):
            return False
         if (self.check_move_through_wall(next_x, next_y)):
            return False
         
         current_x = next_x
         current_y = next_y
         
      return True
   
   def check_move_through_wall(self, state_x, state_y):
      if const.MAZE[state_x][state_y] == 1:
         return True
      return False
   
   def execute(self):
           
       root = Tk()
       ents = self.makeform(root, self.fields, self.door_choices)
          
       b1 = Button(root, text='Add', command=lambda: self.add_data(self.data_list, ents))
       b1.pack(side=LEFT, padx=5, pady=5)
      
       b2 = Button(root, text='Write to file', command=lambda:self.data_reader_and_writer.write(self.data_list))
       b2.pack(side=LEFT, padx=5, pady=5)
      
       root.mainloop()
