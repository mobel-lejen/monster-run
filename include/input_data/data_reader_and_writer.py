import json
from include.input_data.compact_json_encoder import CompactJSONEncoder
import include.const as const

class DataReaderAndWriter:
   def __init__(self):
      self.json_encoder = CompactJSONEncoder()
      self.file_name = const.DATA_FILE_NAME
      
   def write(self, data):
       with open(self.file_name, 'w') as outfile:  
           outfile.write(self.json_encoder.encode(data))

   def read(self):
       data_list = []
       try:
           with open(self.file_name) as json_file:
               data_list = json.load(json_file)
       except:
           data_list = []
       return data_list
