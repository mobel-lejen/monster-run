from .. import const
from include.input_data.data_reader_and_writer import DataReaderAndWriter

class InputWhilePlaying:
   def __init__(self):
      self.data_reader_and_writer = DataReaderAndWriter()
      self.data_list = self.data_reader_and_writer.read()

   def add_data(self, data):
      data_dict = {}
      
      goal_coordinate = data[len(data)-1]
      goal = ""
      if (goal_coordinate == const.NORTH_DOOR):
         goal = "North"
      elif (goal_coordinate == const.SOUTH_DOOR):
         goal = "South"
      elif (goal_coordinate == const.EAST_DOOR):
         goal = "East"
      elif (goal_coordinate == const.WEST_DOOR):
         goal = "West"

      data_dict['Goal'] = goal
      data_dict['Path'] = data

      self.data_list.append(data_dict)
   
   def execute(self, data):
      self.add_data(data)
      self.data_reader_and_writer.write(self.data_list)
