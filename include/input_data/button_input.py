from tkinter import *
import include.const as const
from include.input_data.data_reader_and_writer import DataReaderAndWriter


class ButtonInput(object):

    def __init__(self):
        self.data_reader_and_writer = DataReaderAndWriter()
        self.data_list = self.data_reader_and_writer.read()
        self.data_dict = {"Goal" : "", "Path" : []}
        
        self.master = Tk()
        self.master.configure(background='#E8F0FF')
        self.master.resizable(False, False)
        self.column_span = 1
        self.button_width = 3

        button_row = 0
        button_column = 0
        #Iteration to create multiple buttons.
        for row in range(0, len(const.MAZE)):
            for col in range(0, len(const.MAZE[row])):
                self.ButtonColor(const.MAZE[row][col], button_row, button_column)

                self.CreateAndGridButton(const.MAZE[row][col], button_row, button_column)
                
                button_column += self.column_span
            button_column = 0
            button_row += 1

    def emptyDataDict(self):
        self.data_dict = {"Goal" : "", "Path" : []}
        
    def CreateAndGridButton(self, key, button_row, button_column):
        """Create and grid the button"""
        def cmd(row=button_row, column=button_column, x=key):
            """command for the button"""
            self.click(row, column, x)
            
        b = Button(self.master, height=1, width=self.button_width, borderwidth=2, relief=RAISED,
                   bg= self.background, font=("Helvetica 17 bold"), command=cmd)
        b.grid(row= button_row, column= button_column, columnspan = self.column_span)
    
        
    def ButtonColor(self, key, button_row, button_column):
        if key == const.WALL_VALUE :
            self.background = "#6874E8"
        else:
            self.background = "#FFFFFF"

    def writeData(self):
        self.data_list.append(self.data_dict)
        self.data_reader_and_writer.write(self.data_list)
        self.emptyDataDict()
    
    def click(self, row, column, key):
        data_tuple = (column, row)
        if key == const.WALL_VALUE:
            self.show_error("Can't move through wall")
        elif data_tuple == const.NORTH_DOOR:
            self.data_dict["Goal"] = "North"
            self.writeData()
        elif data_tuple == const.SOUTH_DOOR:
            self.data_dict["Goal"] = "South"
            self.writeData()
        elif data_tuple == const.WEST_DOOR:
            self.data_dict["Goal"] = "West"
            self.writeData()
        elif data_tuple == const.EAST_DOOR:
            self.data_dict["Goal"] = "East"
            self.writeData()
        else:
            self.data_dict["Path"].append(data_tuple)
    
    def show_error(self, message):
       messagebox.showinfo("Error", message)
    
    def execute(self) :
       self.master.mainloop()
