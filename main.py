from include.pygame import K_RETURN, event, key
from include.monster_run import MonsterRun
from include.util import check_quit
from include.state import State
from include.renderer.main_renderer import MainRenderer
from include.renderer.hide_path_renderer import HidePathRenderer
from include.engine.random import RandomEngine
from include.engine.split import SplitEngine
from include.engine.nomove import NoMoveEngine
from include.const import *
from include.predictor.shortest import ShortestPredictor
from include.predictor.ml import MLPredictor
from include.input_data.manual_input import ManualInput
from include.input_data.button_input import ButtonInput
from include.input_data.input_while_playing import InputWhilePlaying
from precompute import get_path_generator
from argparse import ArgumentParser
from os import environ

def parse_args():
    parser = ArgumentParser(description='Play monster runner game.')
    parser.add_argument('-m', '--monster', help='monster movement strategy',
        choices=['random', 'split', 'nomove'], default='split')
    parser.add_argument('-p', '--predictor', help='your targeted exit predictor',
        choices=['shortest', 'ml'], default='ml')
    parser.add_argument('-sp', '--show-path',
        help='show the predicted path you\'re heading in', action='store_true')
    parser.add_argument('-id', '--input-data', help='input data',
        choices=['manual', 'onclick', 'while_playing', 'none'], default='none')
    return parser.parse_args()

if __name__ == "__main__" :
    args = parse_args()

    environ['SDL_VIDEO_CENTERED'] = '1'
    
    if args.input_data == "manual":
        ManualInput().execute()
        exit()

    if args.input_data == "onclick":
        ButtonInput().execute()
        exit()
    
    state = State(MAZE)
    path_generator = get_path_generator()

    if args.predictor == 'shortest':
        predictor = ShortestPredictor(state, path_generator)
    elif args.predictor == 'ml':
        predictor = MLPredictor(state, path_generator)

    if args.show_path:
        renderer = MainRenderer(state)
    else:
        renderer = HidePathRenderer(state)

    if args.monster == "split":
        engine = SplitEngine(state, predictor, path_generator)
    elif args.monster == "random":
        engine = RandomEngine(state)
    else:
        engine = NoMoveEngine(state)
    monster_run = MonsterRun(state, renderer, engine, predictor)

    monster_run.new_game()

    if args.input_data == "while_playing":
        input_data = InputWhilePlaying()
        if monster_run.getState().is_finish():
            input_data.execute(monster_run.getState().get_pos_history(PLAYER_VALUE))
    
    wait_for_newgame = True
    while wait_for_newgame:
        events = event.get()
        keys = key.get_pressed()
        check_quit(events, keys)

        if keys[K_RETURN]:
            monster_run.new_game()
            if args.input_data == "while_playing" and monster_run.getState().is_finish():
                input_data.execute(monster_run.getState().get_pos_history(PLAYER_VALUE))
