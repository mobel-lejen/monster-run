from include.const import *
from include.util import substract_pair
from include.path.path_generator import PathGenerator
from include.state import State
from include.path.tilemove import TileMove
import json
import scipy.io as sio
import numpy as np

def generate_ml_input(pathtile, path_generator):
    NUMBER_OF_LAST_TILE_PICKED = 3
    # Assure pathtile has more than NUMBER_OF_LAST_TILE_PICKED element
    pathtile = [pathtile[0]] * (NUMBER_OF_LAST_TILE_PICKED - len(pathtile) + 1) + pathtile
    pathtile = pathtile[-NUMBER_OF_LAST_TILE_PICKED - 1:]

    prev_tile = tuple(pathtile[0])
    prev_paths = path_generator.paths_to_exit(TileMove(pathtile[0], MOVE_NOWHERE))
    result = []
    for j in range(1, len(pathtile)):
        cur_tile = tuple(pathtile[j])
        cur_move = substract_pair(cur_tile, prev_tile)
        paths_to_exit = path_generator.paths_to_exit(TileMove(cur_tile, cur_move))
        for k, path_to_exit in enumerate(paths_to_exit):
            dif_tile_cost = path_to_exit.tile_cost - prev_paths[k].tile_cost
            result.append(dif_tile_cost)
            result.append(path_to_exit.tile_cost / (ROW_SIZE + COL_SIZE))
            result.append(path_to_exit.move_cost / 20)
        prev_tile = cur_tile
        prev_paths = paths_to_exit
    return result

if __name__ == "__main__":
    EXIT_NUM = {
        'South': 1,
        'North': 2,
        'West': 3,
        'East': 4,
    }
    with open(DATA_PATH_FILE_NAME) as json_file:
        json = json.load(json_file)
        X = []
        y = []
        path_generator = PathGenerator(State(MAZE))
        for i in range(len(json)):
            X.append(generate_ml_input(json[i]['Path'], path_generator))
            y.append([EXIT_NUM[json[i]['Goal']]])
        X = np.matrix(X, dtype=np.double)
        y = np.matrix(y, dtype=np.double)
        sio.savemat('ml/neural-network/train.mat', {'X': X, 'y': y})
